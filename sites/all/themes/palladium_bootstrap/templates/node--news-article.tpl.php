<?php if(!empty($carousel_image)){?>
<div class="row hidden-xs">
    <div class="col-xs-12" style="margin-bottom: 2em;">
        <div>Carousel Image Here</div>
    </div>
</div>
<?php } ?>
<div id="mission-areas-page" class="row">

    <div id="main-content" class="col-md-9">

        <!--<div id="mm_gallery_transcript" class="col-md-12">
             <?php //print($transcript); ?>
         </div>-->
        <div id="mm_gallery_description" class="col-md-12">
            <p>Released: <?php print($release_date); ?></p>
        </div>
        <div id="article_body" class="col-md-12">
            <?php print $body; ?>
    </div>

        <div id="mm_gallery_tags" class="col-md-12">
            <h2>Tags</h2>
            <?php print($tags); ?>
        </div>
        <div class="disclaimer col-md-12"><sup>*</sup>Links and contacts within this release are valid at the time of publication.</div>
    </div>
    <div class="col-md-3">

        <div id="mm_gallery_photographer_data">
            <h3>Contact Information<sup>*</sup></h3>
            <h4 style="font-size:1.1em; color:#000; margin-bottom: 2em;">U.S. Department of the Interior,<br />
                U.S. Geological Survey<br />
                <span style="font-size: .8em;">Office of Communications and Publishing<br />
                12201 Sunrise Valley Dr, MS 119<br />
                Reston, VA 20192</span></h4>
            <?php foreach($contacts as $contact){
                    print('<div class="news_contact">');
                    print('<p class="news_contact_name">'.$contact['name'].'</p>');
                    print('<p><a href="mailto:'.$contact['email'].'">'.$contact['email'].'</a></p>');
                    print('<p>'.$contact['phone'].'</p>');
                    print('<p>'.$contact['title'].'</p>');
                print('<a class="btn btn-success btn-sm" href="#">Request Interview</a>');
                print('</div>');
                }

            ?>

        </div>
       <?php if(!empty($partners)) { ?>}
        <div id="mm_gallery_partner_data">
            <h3>Partners</h3>
            <p><span class="glyphicon glyphicon-tree-conifer" style="font-size: 4em; margin-right: .4em;"></span>Partner 1</p>
            <p><span class="glyphicon glyphicon-tree-conifer" style="font-size: 4em; margin-right: .4em;"></span>Partner 2</p>
        </div>
        <?php } ?>
        <div id="mm_gallery_connect_data">
            <h3>Connect</h3>
            <h4><span class="" style="display:block; display:inline-block; width: 75px; padding: 5px; margin-right: 25px;"><a href="#"><img src="<?php print($rss_icon);?>" style="width:75px;" /></span>Subscribe RSS</a></h4>
            <h4><span class="" style="background-color:#55acee; display:inline-block; width: 75px; padding: 5px; margin-right: 25px;"><a href=""><img src="<?php print($twitter_icon); ?>" style="width:65px;"/></span>Follow USGS News</a></h4>

        </div>
    </div>
</div>