<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$url = preg_replace('/[^a-z0-9_]+/', '_' , strtolower($fields['title']->raw));
$bad_path = $fields['field_webcam_path']->content;
$path = strip_tags($bad_path);
?>

<div class="col-md-2">
    <?php print $fields['field_webcam_thumbail']->content; ?>
</div>
<div class="col-md-10">
    <h4><a href="<?php print $path ?>"><?php print $fields['title']->raw; ?></a></h4>
    <?php print $fields['body']->content; ?>
</div>
