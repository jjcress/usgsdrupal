<?php
/**
 * @file
 * Preprocessors and helper functions for overriding the views templates.
 *
 * This is included in palladium_bootstrap's template.php file
 */

function palladium_bootstrap_preprocess_views_view_unformatted(&$vars) {
}


/**
 * Add variables for the overridden views template.
 *
 * @param unknown $vars
 */
function palladium_bootstrap_preprocess_views_view_fields(&$vars) {
    
	foreach($vars['fields'] as $field_name => $field) {
		$out = '';
		if(!empty($field->separator)) {
			$out .= $field->separator;
		}

		$out .= $field->wrapper_prefix;
		$out .= $field->label_html;
		$out .= $field->content;
		$out .= $field->wrapper_suffix;

		$vars[$field_name] = $out;
	}

	// Because when we first created the content types, multiple fields were created for
	// the image fields.
        
        /*
         * Preprocess the date taken field from the USGS gallery video file type
         */
   
        if(!empty($vars['theme_hook_original'])){
        if($vars['theme_hook_original'] == 'views_view_fields__multimedia_gallery__video' || $vars['theme_hook_original'] == 'views_view_fields__multimedia_gallery__video_thumbnails') {
          //dsm($vars);
            $temp_date = $vars['fields']['field_mm_gallery_video_date_add']->content;
            $raw_date = strip_tags($temp_date);
            if(!empty($raw_date)){
                $timestamp = strtotime($raw_date);
                if($timestamp) {
                    $vars['file_date'] = date('M d, Y', $timestamp);
                } else{
                    $vars['file_date'] = 'Unknown Date'; 
                }
            } else {
                $vars['file_date'] = 'Unknown Date';
            }
        }
        }
       
}

function palladium_bootstrap_preprocess_views_view_field(&$vars) {
	//dsm($vars['row']);
	if($vars['field']->field == 'field_links_to') {
		$options = array(
				'html' => TRUE,
				'query' => drupal_get_destination(),
		);
		$vars['link'] = user_access('administer nodes') ? l('<span class="badge badg-usgs">Edit</span>', "node/{$vars['row']->nid}/edit", $options) : '';
	}


}
