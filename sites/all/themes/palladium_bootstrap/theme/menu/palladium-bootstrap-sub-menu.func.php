<?php

/**
 * Palladium Bootstrap theme wrapper function for the primary menu links.
 */
function palladium_bootstrap_sub_menu(&$variables) {
  return '<ul class="menu-dropdown menu-usgs nav navbar-nav">' . $variables['tree'] . '</ul>';
}